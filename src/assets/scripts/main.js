// Polyfill for using matches() in IE
(function(e){

  e.matches || (e.matches=e.matchesSelector||function(selector){
    var matches = document.querySelectorAll(selector), th = this;
    return Array.prototype.some.call(matches, function(e){
       return e === th;
    });
  });

})(Element.prototype);

const books = JSON.parse(localStorage.getItem('books')) || [
  {
    author: "Булгаков М.А",
    title: "Мастер и Маргарита",
    year: "1967",
    count: "416"
  },
  {
    author: "Достоевский Ф.М",
    title: "Преступление и наказание",
    year: "2016",
    count: "672"
  },
  {
    author: "Толстой Л.Н",
    title: "Анна Кареннина",
    year: "2016",
    count: "864"
  }
]

const addBooks = document.querySelector('.add-book-form');
const booksList = document.querySelector('.books__list');
const resetButton = document.querySelector('#resetButton');
const submitButton = document.querySelector('#submit');

function addBook(e) {
  e.preventDefault();
  const author = (this.querySelector('[name=author]')).value;
  const title = (this.querySelector('[name=title]')).value;
  const year = (this.querySelector('[name=year]')).value;
  const count = (this.querySelector('[name=count]')).value;
  const action = submitButton.dataset.action;
  if (action === 'create') {
    if (!author || !title || !year || !count) {
      alert("Заполните все поля");
      return;
    };
    const book = {
      author,
      title,
      year,
      count
    };

    books.push(book);
    createList(books, booksList);
    localStorage.setItem('books', JSON.stringify(books));
    this.reset()
  } else if (action === 'edit') {
    const index = submitButton.dataset.index
    books[index] = {
      author,
      title,
      year,
      count
    };
    createList(books, booksList);
    localStorage.setItem('books', JSON.stringify(books));
    submitButton.innerHTML = 'Добавить';
    submitButton.dataset.index = ''
    submitButton.dataset.action = 'create'
    resetButton.classList.remove('is-visible');
    this.reset()
  }

}

function createList(booksItems = [], booksList) {
  booksList.innerHTML = booksItems.map((item, i) => {
    return `
    <li class="books__list-item">
      <div class="books__info">
        <span class="books__author"> ${item.author} </span>
        ${item.title}
      </div>
      <div class="books__edit">
        <button id="edit" class="btn btn--icon" type="button" data-index=${i}> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </button>
        <button id="remove" class="btn btn--icon" type="button" data-index=${i}> <i class="fa fa-trash-o" aria-hidden="true"></i> </button>
      </div>
    </li>
    `
  }).join('');
}

function editBook(e) {
  if (!e.target.matches('button')) return;
  const el = e.target;
  const index = el.dataset.index;
  if(el.id === 'remove') {
    books.splice(index, 1)
    localStorage.setItem('books', JSON.stringify(books));
    createList(books, booksList);
  }
  if(el.id === 'edit') {
    const book = books[index];
    submitButton.dataset.action = 'edit';
    submitButton.dataset.index = index;
    submitButton.innerHTML = 'Изменить';
    resetButton.classList.add('is-visible');
    document.querySelector('[name=author]').value = book.author;
    document.querySelector('[name=title]').value = book.title;
    document.querySelector('[name=year]').value = book.year;
    document.querySelector('[name=count]').value = book.count;
  }
}

addBooks.addEventListener('submit', addBook);
booksList.addEventListener('click', editBook);
resetButton.addEventListener('click', function() {
  addBooks.reset();
  submitButton.innerHTML = 'Добавить';
  submitButton.dataset.index = ''
  submitButton.dataset.action = 'create'
  this.classList.remove('is-visible');
})

createList(books, booksList)
